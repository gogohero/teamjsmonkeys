console.log('Validator.js loaded!');

var Validator = (function() {
    var _error = false;
    var _errorObj = {};

    var publicApi = {
        'passed': function() {
            return !_error;
        },
        'errors': function() {
            return _errorObj;
        },
        'check': _check
    };
    return publicApi;

    function _check (obj, rules) {
        _error = false;
        _errorObj = {};

        if (obj === null || obj === undefined) {
            _error = true;
        }

        for (var field in obj) {
            if (rules[field] !== undefined) {
                _checkRules(field, obj[field], rules[field]);
            }
        }

        function _checkRules(field, input, rules) {

            for (var rule in rules) {
                switch (rule) {

                    case 'required':
                        if (input.length === 0) {
                            _error = true;
                            _errorObj[field] = rules[rule].err;
                            return;
                        }
                        break;

                    case 'min-size':
                        if (input.length < rules[rule].val) {
                            _error = true;
                            _errorObj[field] = rules[rule].err;
                            return;
                        }
                        break;

                    case 'max-size':
                        if (input.length > rules[rule].val) {
                            _error = true;
                            _errorObj[field] = rules[rule].err;
                            return;
                        }
                        break;

                    case 'regex':
                        if (input.search(rules[rule].val) === -1) {
                            _error = true;
                            _errorObj[field] = rules[rule].err;
                            return;
                        }
                        break;

                    case 'exists':
                        if (rules[rule].val.call(self,input)) {
                            _error = true;
                            _errorObj[field] = rules[rule].err;
                            return;
                        }
                        break;

                    default:
                        break;
                }
            }
        }
    }

})();