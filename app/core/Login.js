console.log('Login.js loaded!');

var Login = (function() {
    var _errors = null;
    var _loggedUser=null;

    var publicAPI = {
        'login': _login,
        'loggedUser' : function(){
            return _loggedUser;
        },
        'errors': function() {
            return _errors;
        }
    };
    return publicAPI;

    function _login (username, password) {
        if (typeof(Validator) === 'undefined') {
            throw new Error('Missing dependency: Validator! Can\'t continue.');
        }

        if (typeof(ErrorTextProvider) === 'undefined') {
            throw new Error('Missing dependency: ErrorTextProvider! Can\'t continue.');
        }

        var info = {
            'username': username,
            'password': password
        };

        var rules = {
            'username': {
                'required': { val: true, err: 100 }
            },
            'password': {
                'required': { val: true, err: 110 }
            }
        };

        Validator.check(info, rules);

        if (Validator.passed()) {
            debugger;
            /*wrong! - async call from db, if not working!*/return API.user.check(username, password, function(result) {
                debugger;


                if(result.length  > 0){

                    _loggedUser = username;
                    _errors = {};

                    //todo: Redirect to HomePage or this need to be done by the Router?

                    return true;

                } else{
                    var key = 120;

                    _errors = { key: ErrorTextProvider.getErrorText(key)};

                    return false;
                }
            });

            

        } else {
            var errors = Validator.errors();
            var txt = {};

            for (var key in errors) {
                txt[key] = ErrorTextProvider.getErrorText(errors[key]);
            }

            _errors = txt;

            return false;
        }
    }
})();