console.log('Registration.js loaded!');

var Registration = (function() {
    var _errors = null;

    var publicAPI = {
        'register': _register,
        'errors': function() {
            return _errors;
        }
    };
    return publicAPI;

    function _register (username, password) {
        if (typeof(Validator) === 'undefined') {
            throw new Error('Missing dependency: Validator! Can\'t continue.');
        }

        if (typeof(ErrorTextProvider) === 'undefined') {
            throw new Error('Missing dependency: ErrorTextProvider! Can\'t continue.');
        }

        var info = {
            'username': username,
            'password': password
        };

        var rules = {
            'username': {
                'required': { val: true, err: 100 },
                'min-size': { val: 6, err: 101 },
                'max-size': { val: 16, err: 102 },
                'regex': { val: /^[a-z]([a-z0-9]*(_)?[a-z0-9]+)+$/i, err: 103 },
                'exists': {val: API.user.usernameExists, err: 104}
            },
            'password': {
                'required': { val: true, err: 110 },
                'min-size': { val: 6, err: 111 },
                'max-size': { val: 20, err: 112 },
                'regex': { val: /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).*$/, err: 113 }
            }
        };

        Validator.check(info, rules);

        if (Validator.passed()) {

            _errors = {};

            API.user.add(username, password);

            return true;

        } else {
            var errors = Validator.errors();
            var txt = {};

            for (var key in errors) {
                txt[key] = ErrorTextProvider.getErrorText(errors[key]);
            }

            _errors = txt;

            return false;
        }
    }
})();