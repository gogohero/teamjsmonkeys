console.log('ErrorTextProvider.js loaded!');

var ErrorTextProvider = (function() {

    var publicAPI = {
        getErrorText: _get
    };
    return publicAPI;


    function _get (errorNum) {

        if (errorNum >= 100 && errorNum < 200) {
            switch (errorNum) {
                case 100:
                    return 'Username required!';
                case 101:
                case 102:
                    return 'Username must be between 6 and 16 simbols!';
                case 103:
                    return 'Username must be a-z A-z 0-9 _ , must start with letter, can\'t have consecutive _ and finish with _!';
                case 104:
                    return 'Username already in use!';

                case 110:
                    return 'Password is required!';
                case 111:
                case 112:
                    return 'Password must be between 6 and 16 simbols!';
                case 113:
                    return 'Password must have at least one A-Z, one a-z and one 0-9!';

                case 120:
                    return 'Wrong username or password'
                default:
                    return 'Error 404 - Error not found!';
            }
        }
    }

})();